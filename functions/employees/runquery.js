const mysql = require('mysql');
const config = require('./config.json');
const pool  = mysql.createPool({
    host     : config.dbhost,
    port     : config.dbport,
    user     : config.dbuser,
    password : config.dbpassword,
    database : config.dbname
});

exports.runQuery = async function(sql, params) {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            connection.query(sql, params, (err, results) => {
                if (err){
                    reject(err);
                }
                connection.release();
                resolve(results);
            });
        });
    });
};
