const search = require('elasticsearch');
const config = require('./config.json');
const client = new search.Client({
    hosts: [config.eshost + ':' + config.esport]
});

module.exports = client;
