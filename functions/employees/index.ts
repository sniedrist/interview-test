export {default as employeesPost} from './post';
export {default as employeesGet} from './get';
export {default as employeesSearch} from './search';
export {default as employeesDelete} from './delete';
