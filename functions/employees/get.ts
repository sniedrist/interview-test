'use strict';

import {APIGatewayEvent} from 'aws-lambda';

const db = require('./runquery');

export default async function (event: APIGatewayEvent): Promise<any> {
    console.log(event);
    const responseHeaders = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
    }
    let result = {};
    try {
        let query = "SELECT empl_id, name FROM employees WHERE empl_id = ? ";
        result = await db.runQuery(query, event.pathParameters.id);
        console.log(result);
    }
    catch (err) {
        return {
            statusCode: '400',
            body: JSON.stringify({
                error: err.sqlMessage
            })
        }
    }
    if (result[0])
    {
        return {
            statusCode: '200',
            headers: responseHeaders,
            body: JSON.stringify({
                empl_id: result[0].empl_id,
                name: result[0].name
            })
        }
    }
    else
    {
        return {
            statusCode: '404'
        }
    }
}

