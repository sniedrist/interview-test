'use strict';

import {APIGatewayEvent} from 'aws-lambda';
    
const db = require('./runquery');
const client = require('./searchclient');

export default async function (event: APIGatewayEvent): Promise<any> {
    console.log(event);
    const post = {empl_id:event.queryStringParameters.empl_id, name:event.queryStringParameters.name};
    const getUrl = "http://" + event.headers.Host + "/employees/" + post.empl_id;
    const responseHeaders = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',  
        'Access-Control-Allow-Credentials': true,
        'Location': getUrl
    }

    let result = {};
    try {
        let query = "INSERT INTO employees SET ? ";
        result = await db.runQuery(query, post);
        console.log(result);
    }
    catch (err) {
        console.log(err);
        return {
            statusCode: '400',
            headers: responseHeaders,
            body: JSON.stringify({
                error: err.sqlMessage
            })
        }
    }

    try {
        let elasticIndex = await client.index({
            index: 'employees',
            type: 'employee',
            id: result.insertId,
            body: {
                empl_id : post.empl_id,
                name    : post.name
            }
        });
        console.log(elasticIndex);
    }
    catch (err) {
        console.log(err);
        return {
            statusCode: '400',
            headers: responseHeaders,
            body: JSON.stringify({
                error: err.message
            })
        }
    }

    return {
        statusCode: '201',
        headers : responseHeaders,
        body: JSON.stringify({
            empl_id : post.empl_id,
            name    : post.name
        })
    }
}

