'use strict';

import {APIGatewayEvent} from 'aws-lambda';

const db = require('./runquery');

export default async function (event: APIGatewayEvent): Promise<any> {
    console.log(event);
    let result = {};
    try{
        let query = "DELETE FROM employees WHERE empl_id = ? ";
        result = await db.runQuery(query, event.pathParameters.id);
        console.log(result);
    }
    catch (err) {
        return {
            statusCode: '400',
            body: JSON.stringify({
                error: err.sqlMessage
            })
        }
    }
  
    if (result.affectedRows === 0)
    {
        return {
            statusCode: '404'
        }
    }

    return {
        statusCode: '204',
    }
}

