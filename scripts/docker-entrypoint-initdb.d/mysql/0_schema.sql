# Add your initial schema here....
CREATE TABLE IF NOT EXISTS `employees`.`employees` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `empl_id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `empl_id_UNIQUE` (`empl_id` ASC))
ENGINE = InnoDB
