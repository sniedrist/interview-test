'use strict';

import {APIGatewayEvent} from 'aws-lambda';

const client = require('./searchclient');

export default async function (event: APIGatewayEvent): Promise<any> {
    let records = [];
    let employees = [];

    var { _scroll_id, hits } = await client.search({
        index: 'employees',
        scroll: '10s',
        body: {
            query: {
                "match_all": {}
            }
        }
    });

    while(hits && hits.hits.length) {
        records.push(...hits.hits);

        var { _scroll_id, hits } = await client.scroll({
            scrollId: _scroll_id,
            scroll: '10s'
        });
    }

    console.log(records);

    records.forEach((r) => {
        employees.push(r._source);
    });

    return {
        statusCode: '200',
        body: JSON.stringify(employees);
    }
}
